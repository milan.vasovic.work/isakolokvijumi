package app.dto;

import java.time.LocalDateTime;

import app.model.Racun;

public class RacunDTO {
	private Long id;
	private LocalDateTime datumIzdavanja;
	
	private Long kupac;
	private Long kasir;
	private Long kasa;
	
	public RacunDTO() {
		super();
	}
	
	public RacunDTO(Racun racun) {
		super();
		this.id = racun.getId();
		this.datumIzdavanja = racun.getDatumIzdavanja();
		this.kupac = racun.getKupac().getId();
		this.kasir = racun.getKasir().getId();
		this.kasa = racun.getKasa().getId();
	}

	public RacunDTO(Long id, LocalDateTime datumIzdavanja, Long kupac, Long kasir, Long kasa) {
		super();
		this.id = id;
		this.datumIzdavanja = datumIzdavanja;
		this.kupac = kupac;
		this.kasir = kasir;
		this.kasa = kasa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDatumIzdavanja() {
		return datumIzdavanja;
	}

	public void setDatumIzdavanja(LocalDateTime datumIzdavanja) {
		this.datumIzdavanja = datumIzdavanja;
	}

	public Long getKupac() {
		return kupac;
	}

	public void setKupac(Long kupac) {
		this.kupac = kupac;
	}

	public Long getKasir() {
		return kasir;
	}

	public void setKasir(Long kasir) {
		this.kasir = kasir;
	}

	public Long getKasa() {
		return kasa;
	}

	public void setKasa(Long kasa) {
		this.kasa = kasa;
	}
}
