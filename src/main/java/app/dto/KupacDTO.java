package app.dto;

import java.util.ArrayList;
import java.util.List;

import app.model.Kupac;
import app.model.Racun;

public class KupacDTO {
	private Long id;
	private String ime;
	private String prezime;
	private List<RacunDTO> racuni = new ArrayList<RacunDTO>();
	
	public KupacDTO() {
		super();
	}
	
	public KupacDTO(Kupac kupac) {
		super();
		this.id = kupac.getId();
		this.ime = kupac.getIme();
		this.prezime = kupac.getPrezime();
		for(Racun r : kupac.getRacuni()) {
			this.racuni.add(new RacunDTO(r));
		}
	}

	public KupacDTO(Long id, String ime, String prezime) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public List<RacunDTO> getRacuni() {
		return racuni;
	}

	public void setRacuni(List<RacunDTO> racuni) {
		this.racuni = racuni;
	}	
}
