package app.dto;

import java.util.ArrayList;
import java.util.List;

import app.model.Kasa;
import app.model.Racun;

public class KasaDTO {
	private Long id;
	private String sifra;
	
	private List<RacunDTO> racuni = new ArrayList<RacunDTO>();
	
	public KasaDTO() {
		super();
	}
	
	public KasaDTO(Kasa kasa) {
		super();
		this.id = kasa.getId();
		this.sifra = kasa.getSifra();
		for(Racun r : kasa.getRacuni()) {
			this.racuni.add(new RacunDTO(r));
		}
	}

	public KasaDTO(Long id, String sifra) {
		super();
		this.id = id;
		this.sifra = sifra;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public List<RacunDTO> getRacuni() {
		return racuni;
	}

	public void setRacuni(List<RacunDTO> racuni) {
		this.racuni = racuni;
	}
}
