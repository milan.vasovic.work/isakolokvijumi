package app.dto;

import app.model.Kasir;

public class KasirDTO {
	private Long id;
	private String sifra;
	private String ime;
	private String prezime;

	public KasirDTO() {
		super();
	}
	
	public KasirDTO(Kasir kasir) {
		super();
		this.id = kasir.getId();
		this.ime = kasir.getIme();
		this.prezime = kasir.getPrezime();
	}

	public KasirDTO(Long id, String sifra, String ime, String prezime) {
		super();
		this.id = id;
		this.sifra = sifra;
		this.ime = ime;
		this.prezime = prezime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
}
