package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Racun;

@Repository
public interface RacunRepository extends PagingAndSortingRepository<Racun, Long>{

}
