package app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.model.Kasa;

@Repository
public interface KasaRepository extends PagingAndSortingRepository<Kasa, Long> {
//	@Query("SELECT r FROM kasa, racun r, kupac WHERE kasa.id = racun.kasa_id and kasir.id = racun.kasir_id and kupac.id = racun.kupac_id and kupac.id = :par1 and kasir.id = :par2")
//	public Iterable<Kasa> findAllWhereKupacAndKasir(@Param("par1") Long kupac, @Param("par2") Long kasir);
}
