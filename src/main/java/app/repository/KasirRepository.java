package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Kasir;

@Repository
public interface KasirRepository extends PagingAndSortingRepository<Kasir, Long>{

}
