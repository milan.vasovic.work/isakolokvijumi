package app.repository;

import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.model.Kupac;

@Repository
public interface KupacRepository extends PagingAndSortingRepository<Kupac, Long>{
//	@Query("SELECT k FROM kupac k WHERE k.datum_izdavanja = :datum")
//	public Iterable<Kupac> findAllWhereDatum(@Param("datum") LocalDateTime datum);
}
