package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Kasir;
import app.repository.KasirRepository;

@Service
public class KasirService {
	@Autowired
	KasirRepository kasirRepository;
	
	public KasirService() {
		super();
	}
	
	public Iterable<Kasir> findAll(){
		return kasirRepository.findAll();
	}
	
	public Page<Kasir> findAll(Pageable pageable) {
		return kasirRepository.findAll(pageable);
	}
	
	public Kasir findOne(Long id) {
		return kasirRepository.findById(id).orElse(null);
		
	}
	
	public void remove(Long id) {
		kasirRepository.deleteById(id);
	}
	
	public Kasir save(Kasir kasir) {
		return kasirRepository.save(kasir);
	}
}
