package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Racun;
import app.repository.RacunRepository;

@Service
public class RacunService {
	@Autowired
	RacunRepository racunRepository;
	
	public RacunService() {
		super();
	}
	
	public Iterable<Racun> findAll(){
		return racunRepository.findAll();
	}
	
	public Page<Racun> findAll(Pageable pageable) {
		return racunRepository.findAll(pageable);
	}
	
	public Racun findOne(Long id) {
		return racunRepository.findById(id).orElse(null);
		
	}
	
	public void remove(Long id) {
		racunRepository.deleteById(id);
	}
	
	public Racun save(Racun student) {
		return racunRepository.save(student);
	}
}
