package app.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Kupac;
import app.repository.KupacRepository;

@Service
public class KupacService {
	@Autowired
	KupacRepository kupacRepository;
	
	public KupacService() {
		super();
	}
	
	public Iterable<Kupac> findAll(){
		return kupacRepository.findAll();
	}
	
	public Iterable<Kupac> findAllWhereDatum(LocalDateTime datum){
		return kupacRepository.findAll();
	}
	
	public Page<Kupac> findAll(Pageable pageable) {
		return kupacRepository.findAll(pageable);
	}
	
	public Kupac findOne(Long id) {
		return kupacRepository.findById(id).orElse(null);
		
	}
	
	public void remove(Long id) {
		kupacRepository.deleteById(id);
	}
	
	public Kupac save(Kupac kupac) {
		return kupacRepository.save(kupac);
	}
}

