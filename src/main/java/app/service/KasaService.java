package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Kasa;
import app.repository.KasaRepository;

@Service
public class KasaService {
	@Autowired
	KasaRepository kasaRepository;
	
	public KasaService() {
		super();
	}
	
	public Iterable<Kasa> findAllWhereKupacAndKasir(Long kupac,Long kasir){
		return kasaRepository.findAll();
	}
	
	public Iterable<Kasa> findAll(){
		return kasaRepository.findAll();
	}
	
	public Page<Kasa> findAll(Pageable pageable) {
		return kasaRepository.findAll(pageable);
	}
	
	public Kasa findOne(Long id) {
		return kasaRepository.findById(id).orElse(null);
		
	}
	
	public void remove(Long id) {
		kasaRepository.deleteById(id);
	}
	
	public Kasa save(Kasa kasa) {
		return kasaRepository.save(kasa);
	}
}