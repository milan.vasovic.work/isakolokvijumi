package app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.dto.KasirDTO;
import app.model.Kasir;
import app.service.KasirService;


@Controller
@RequestMapping(path = "/api/kasir")
@CrossOrigin
public class KasirController {
	@Autowired
	private KasirService kasirService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<KasirDTO>> getAll() {
		List<KasirDTO> kasiri = new ArrayList<KasirDTO>();
		for(Kasir k : kasirService.findAll()) {
			kasiri.add(new KasirDTO(k));
		}
		return new ResponseEntity<Iterable<KasirDTO>>(kasiri, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<KasirDTO> getKasir(@PathVariable("id") Long id) {
		Kasir kasir = kasirService.findOne(id);
		KasirDTO targetKasir = new KasirDTO(kasir);
		if (targetKasir != null) {
			return new ResponseEntity<KasirDTO>(targetKasir, HttpStatus.OK);
		}
		return new ResponseEntity<KasirDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Kasir> createKasir(@RequestBody Kasir noviKasir) {
		if (kasirService.findOne(noviKasir.getId()) != null) {
			return new ResponseEntity<Kasir>(HttpStatus.CONFLICT);
		}
		kasirService.save(noviKasir);
		return new ResponseEntity<Kasir>(noviKasir, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Kasir> updateKasir(@PathVariable("id") Long id,
			@RequestBody Kasir podaciKasir) {
		Kasir kasir = kasirService.findOne(id);
		if (kasir == null) {
			return new ResponseEntity<Kasir>(HttpStatus.NOT_FOUND);
		}
		kasir.setIme(podaciKasir.getIme());
		kasir.setSifra(podaciKasir.getSifra());
		kasir.setPrezime(podaciKasir.getPrezime());
		kasirService.save(kasir);
		return new ResponseEntity<Kasir>(kasir, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteKasir(@PathVariable("id") Long id) {
		if (kasirService.findOne(id) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		kasirService.remove(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
