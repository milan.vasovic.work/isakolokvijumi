package app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import app.dto.KasaDTO;
import app.model.Kasa;
import app.service.KasaService;

@Controller
@RequestMapping(path = "/api/kasa")
@CrossOrigin
public class KasaController {
	@Autowired
	private KasaService kasaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<KasaDTO>> getAll() {
		List<KasaDTO> kasa = new ArrayList<KasaDTO>();
		for(Kasa k : kasaService.findAll()) {
			kasa.add(new KasaDTO(k));
		}
		return new ResponseEntity<Iterable<KasaDTO>>(kasa, HttpStatus.OK);
	}
	
//	@RequestMapping(path = "", method = RequestMethod.GET)
//	public ResponseEntity<Iterable<KasaDTO>> getAllWhereKupacAndKasir(@RequestParam(name = "kupac", required = false) Long kupac,
//			@RequestParam(name="kasir", required = false) Long kasir) {
//		List<KasaDTO> kasa = new ArrayList<KasaDTO>();
//		for(Kasa k : kasaService.findAllWhereKupacAndKasir(kupac,kasir)) {
//			kasa.add(new KasaDTO(k));
//		}
//		return new ResponseEntity<Iterable<KasaDTO>>(kasa, HttpStatus.OK);
//	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<KasaDTO> getKasa(@PathVariable("id") Long id) {
		Kasa kasa = kasaService.findOne(id);
		KasaDTO targetKasa = new KasaDTO(kasa);
		if (targetKasa != null) {
			return new ResponseEntity<KasaDTO>(targetKasa, HttpStatus.OK);
		}
		return new ResponseEntity<KasaDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Kasa> createKasa(@RequestBody Kasa novaKasa) {
		if (kasaService.findOne(novaKasa.getId()) != null) {
			return new ResponseEntity<Kasa>(HttpStatus.CONFLICT);
		}
		kasaService.save(novaKasa);
		return new ResponseEntity<Kasa>(novaKasa, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Kasa> updateKasa(@PathVariable("id") Long id,
			@RequestBody Kasa podaciKasa) {
		Kasa kasa = kasaService.findOne(id);
		if (kasa == null) {
			return new ResponseEntity<Kasa>(HttpStatus.NOT_FOUND);
		}
		kasa.setSifra(podaciKasa.getSifra());
		kasaService.save(kasa);
		return new ResponseEntity<Kasa>(kasa, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteKasa(@PathVariable("id") Long id) {
		if (kasaService.findOne(id) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		kasaService.remove(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
