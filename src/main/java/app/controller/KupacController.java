package app.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import app.dto.KupacDTO;
import app.model.Kupac;
import app.service.KupacService;

@Controller
@RequestMapping(path = "/api/kupac")
@CrossOrigin
public class KupacController {
	@Autowired
	private KupacService kupacService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<KupacDTO>> getAll() {
		List<KupacDTO> kupci = new ArrayList<KupacDTO>();
		for(Kupac k : kupacService.findAll()) {
			kupci.add(new KupacDTO(k));
		}
		return new ResponseEntity<Iterable<KupacDTO>>(kupci, HttpStatus.OK);
	}
	
//	@RequestMapping(path = "", method = RequestMethod.GET)
//	public ResponseEntity<Iterable<KupacDTO>> getAllWhereKupacAndKasir(@RequestParam(name = "datum", required = false) LocalDateTime datum) {
//		List<KupacDTO> kupci = new ArrayList<KupacDTO>();
//		for(Kupac k : kupacService.findAllWhereDatum(datum)) {
//			kupci.add(new KupacDTO(k));
//		}
//		return new ResponseEntity<Iterable<KupacDTO>>(kupci, HttpStatus.OK);
//	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<KupacDTO> getKupac(@PathVariable("id") Long id) {
		Kupac kupac = kupacService.findOne(id);
		KupacDTO targetKupac = new KupacDTO(kupac);
		if (targetKupac != null) {
			return new ResponseEntity<KupacDTO>(targetKupac, HttpStatus.OK);
		}
		return new ResponseEntity<KupacDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Kupac> createKupac(@RequestBody Kupac noviKupac) {
		if (kupacService.findOne(noviKupac.getId()) != null) {
			return new ResponseEntity<Kupac>(HttpStatus.CONFLICT);
		}
		kupacService.save(noviKupac);
		return new ResponseEntity<Kupac>(noviKupac, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Kupac> updateKasa(@PathVariable("id") Long id,
			@RequestBody Kupac podaciKupac) {
		Kupac kupac = kupacService.findOne(id);
		if (kupac == null) {
			return new ResponseEntity<Kupac>(HttpStatus.NOT_FOUND);
		}
		kupac.setIme(podaciKupac.getIme());
		kupac.setPrezime(podaciKupac.getPrezime());
		kupacService.save(kupac);
		return new ResponseEntity<Kupac>(kupac, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteKupac(@PathVariable("id") Long id) {
		if (kupacService.findOne(id) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		kupacService.remove(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
