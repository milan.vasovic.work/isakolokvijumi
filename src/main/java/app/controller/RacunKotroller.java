package app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.dto.RacunDTO;
import app.model.Racun;
import app.service.RacunService;

@Controller
@RequestMapping(path = "/api/racun")
@CrossOrigin
public class RacunKotroller {
	@Autowired
	private RacunService racunService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<RacunDTO>> getAll() {
		List<RacunDTO> racuni = new ArrayList<RacunDTO>();
		for(Racun r : racunService.findAll()) {
			racuni.add(new RacunDTO(r));
		}
		return new ResponseEntity<Iterable<RacunDTO>>(racuni, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<RacunDTO> getRacun(@PathVariable("id") Long id) {
		Racun racun = racunService.findOne(id);
		RacunDTO targetRacun = new RacunDTO(racun);
		if (targetRacun != null) {
			return new ResponseEntity<RacunDTO>(targetRacun, HttpStatus.OK);
		}
		return new ResponseEntity<RacunDTO>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Racun> createRacun(@RequestBody Racun noviRacun) {
		if (racunService.findOne(noviRacun.getId()) != null) {
			return new ResponseEntity<Racun>(HttpStatus.CONFLICT);
		}
		racunService.save(noviRacun);
		return new ResponseEntity<Racun>(noviRacun, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Racun> updateRacun(@PathVariable("id") Long id,
			@RequestBody Racun podaciRacun) {
		Racun racun = racunService.findOne(id);
		if (racun == null) {
			return new ResponseEntity<Racun>(HttpStatus.NOT_FOUND);
		}
		racun.setDatumIzdavanja(podaciRacun.getDatumIzdavanja());
		racun.setKasa(podaciRacun.getKasa());
		racun.setKasir(podaciRacun.getKasir());
		racun.setKupac(podaciRacun.getKupac());
		racunService.save(racun);
		return new ResponseEntity<Racun>(racun, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteRacun(@PathVariable("id") Long id) {
		if (racunService.findOne(id) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		racunService.remove(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
